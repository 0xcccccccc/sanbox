# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.2.0](https://gitlab.com/0xcccccccc/sanbox/compare/v2.1.0...v2.2.0) (2021-07-06)


### Features

* **multi1:** multi feature 1 ([415dd8c](https://gitlab.com/0xcccccccc/sanbox/commit/415dd8c3145a57776e363f5f51544174af1a4045))

## [2.1.0](https://gitlab.com/0xcccccccc/sanbox/compare/v2.0.0...v2.1.0) (2021-07-06)


### Features

* **bar:** we can now bar ([4ba81da](https://gitlab.com/0xcccccccc/sanbox/commit/4ba81da2c5bf1a1f6c13f4ee8856b79ce63d31e8))
* **foo:** we can now foo ([8a9fb46](https://gitlab.com/0xcccccccc/sanbox/commit/8a9fb46e6013b89386d6a51a848b5b66e965e55e))

## 2.0.0 (2021-07-06)


### ⚠ BREAKING CHANGES

* schuss

### Features

* added some wired stuff ([d488ffe](https://gitlab.com/0xcccccccc/sanbox/commit/d488ffe378e227097a1e731b2d6ea920e6ab0ffa))
* **blaa:** schuss ([654ea3b](https://gitlab.com/0xcccccccc/sanbox/commit/654ea3b017f011f7d11ead5fd5516e0d00491d58))
* **cool:** added cool feature ([dfd9595](https://gitlab.com/0xcccccccc/sanbox/commit/dfd95954957f9b9209ac08aaba5d4178f1e6577f))
* **ping:** blaa bluipp ([f2a4fbe](https://gitlab.com/0xcccccccc/sanbox/commit/f2a4fbecdb496b0200492cdb729aea8224b3a5c7))
* **README:** added sinnlos eintrag ([e2ff88b](https://gitlab.com/0xcccccccc/sanbox/commit/e2ff88b4af5166ac18e5b95577c069f5a376f8a7))
* **README:** foo bar ([3a58ee4](https://gitlab.com/0xcccccccc/sanbox/commit/3a58ee4ff437948fd86880b34fdb4004d0c47677))

## 0.0.1 

### Initial version